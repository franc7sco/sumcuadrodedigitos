package sumadedigitosambosmetodos;

import java.awt.BorderLayout;
import java.security.SecureRandom;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class PanelPrincipal extends javax.swing.JPanel {

    private ArrayList<Integer[]> numeros;

    private int maximo = 0;
    private int minimo = 0;
    private int iguales = 0;
    private int evaluados = 0;
    private int random = 0;
    private String resultados = "";
    private SecureRandom sr = new SecureRandom();
    private boolean porDeterminista = false;

    private Numeros ventanaPadre;

    public PanelPrincipal(Numeros ventanaPadre) {
        initComponents();
        this.ventanaPadre = ventanaPadre;
        cbTipoG.removeAllItems();
        cbTipoG.addItem("Lineal");
        cbTipoG.addItem("Barras");
        cbTipoAccion.removeAllItems();
        cbTipoAccion.addItem("Determinista");
        cbTipoAccion.addItem("Estocastico");

        cbTipoG.setSelectedIndex(0);
        cbTipoAccion.setSelectedIndex(0);

    }

    private void inicializarDatos() {
        if (esCorrectaLaEntrada()) {
            btnGraficar.setEnabled(false);
            iniciarArreglos();
            llenarArreglo();
            boolean continuar = true;
            iguales = 0;
            evaluados = 0;
            resultados = "";

            while (continuar) {
                porDeterminista = false;
                ArrayList<Integer[]> B;
                int rango = ragoBusqueda(maximo, minimo);

                if (porDeterminista || cbTipoAccion.getSelectedIndex() == 0) {// analiza toda la muestra original ó restante
                    continuar = false;
                    int j = maximo - minimo;
                    //System.out.println("Determinista de " + minimo + "a " + maximo + "Evaluados: " + evaluados);
                    for (int i = 0; i < j; i++) {
                        evaluados++;
                        //System.out.print("Número: " + numeros.get(i));
                        int digitos[] = numeroADigitos(numeros.get(i)[0]);//Crea digitos del número
                        numeros.get(i)[1] = cuadradoYSuma(digitos);//almacenaSuma

                        //System.out.print(" ,Suma de Digitos al cuadrado: " + suma);
                        if (numeros.get(i)[0] == numeros.get(i)[1] && numeros.get(i)[2] != 1) { // ## NUMERO IGUAL A SUMA
                            iguales++;
                            resultados += "Numero : " + numeros.get(i)[0] + ", suma: " + numeros.get(i)[1];
                            resultados += "\n";
                            //System.out.println("Iguales cuando : " + numeros.get(i)[0] + " con suma" + numeros.get(i)[1]);
                        } else {
                            //System.out.println();
                        }
                        numeros.get(i)[2] = 1;//visitado

                    }
                    //System.out.println("Fin determinista");

                } else {
                    // System.out.println("Busqueda estocastiga en un rango de " + rango);
                    B = new ArrayList<Integer[]>(rango + 1);
                    B.clear();
                    //saca una muestra del 10% aproximadamente , para evaluar donde seguir buscando
                    //o descartar malas opciones de busqueda

                    for (int i = 0; i <= rango; i++) {
                        int posicionNumeroEnLista = 0;
                        random = sr.nextInt((maximo + 1) - minimo) + minimo; // con esto sale el numero
                        // random = sr.nextInt((maximo+1) - minimo); // con esto sale una posicion en el arreglo
                        if (minimo == 0) { // recupera la posicion del numero random en la lista
                            posicionNumeroEnLista = random;
                        } else {
                            posicionNumeroEnLista = random - minimo;
                        }
                        if (numeros.get(posicionNumeroEnLista)[2] == 1) {//numero(estado),Ya Visitado.
                            //i--;//para iterari buscar otro numero              //## posible falla

                            //implementacion 2  
                            //volver a generar un numero hasta que sea nueva visita
                           
                        } else {
                            evaluados++;
                            int digitos[] = numeroADigitos(numeros.get(posicionNumeroEnLista)[0]);//Crea digitos del número
                            numeros.get(posicionNumeroEnLista)[1] = cuadradoYSuma(digitos);//almacenaSuma

                            if (numeros.get(posicionNumeroEnLista)[0] == numeros.get(posicionNumeroEnLista)[1] && numeros.get(posicionNumeroEnLista)[2] == 0) { // ## NUMERO IGUAL A SUMA
                                iguales++;
                                resultados += "Numero : " + numeros.get(posicionNumeroEnLista)[0] + ", suma: " + numeros.get(posicionNumeroEnLista)[1];
                                resultados += "\n";
                                //System.out.println("Iguales cuando : " + numeros.get(posicionNumeroEnLista)[0] + " con suma" + numeros.get(posicionNumeroEnLista)[1]);
                            }
                            numeros.get(posicionNumeroEnLista)[2] = 1;//visitado

                            int diferencia = numeros.get(posicionNumeroEnLista)[0] - numeros.get(posicionNumeroEnLista)[1];
                            Integer b[] = new Integer[]{
                                numeros.get(posicionNumeroEnLista)[0], // numero
                                diferencia, //diferencia entre el numero y su suma
                                posicionNumeroEnLista // posicion en el arreglo original
                            };
                            B.add(b);

                        }

                    }

                    //Evaluar nuevo maximo
                    int diferenciamin = 0; // asumimos que el primero es el que es el maximo    ## posible error
                    int indice = 0;

                    for (int i = 0; i < B.size(); i++) {
                        if (i == 0) {
                            diferenciamin = B.get(0)[1];
                        } else {
                            if (B.get(i)[1] < diferenciamin) {
                                diferenciamin = B.get(i)[1];
                                indice = i;
                            }

                        }

                    }
                    if (B.size() == 0) {

                    } else {
                        maximo = numeros.get(B.get(indice)[2])[0];
                    }

                }

            }
            tFcoincidencias.setText("" + iguales);
            tFevaluados.setText("" + evaluados);
            if (resultados.equals("")) {
                txtAreaResultados.setText("SIN RESULTADOS");
            } else {
                txtAreaResultados.setText(resultados);
            }
            btnGraficar.setEnabled(true);

        }// alcance cuando los datos estan bien y se puede continuar
        else {
            btnGraficar.setEnabled(false);
        }

    }

    private Integer[] evaluarResultado(int posicionNumeroEnLista) {

        evaluados++;
        int digitos[] = numeroADigitos(numeros.get(posicionNumeroEnLista)[0]);//Crea digitos del número
        numeros.get(posicionNumeroEnLista)[1] = cuadradoYSuma(digitos);//almacenaSuma

        if (numeros.get(posicionNumeroEnLista)[0] == numeros.get(posicionNumeroEnLista)[1] && numeros.get(posicionNumeroEnLista)[2] == 0) { // ## NUMERO IGUAL A SUMA
            iguales++;
            resultados += "Numero : " + numeros.get(posicionNumeroEnLista)[0] + ", suma: " + numeros.get(posicionNumeroEnLista)[1];
            resultados += "\n";
            //System.out.println("Iguales cuando : " + numeros.get(posicionNumeroEnLista)[0] + " con suma" + numeros.get(posicionNumeroEnLista)[1]);
        }
        numeros.get(posicionNumeroEnLista)[2] = 1;//visitado

        int diferencia = numeros.get(posicionNumeroEnLista)[0] - numeros.get(posicionNumeroEnLista)[1];
        Integer b[] = new Integer[]{
            numeros.get(posicionNumeroEnLista)[0], // numero
            diferencia, //diferencia entre el numero y su suma
            posicionNumeroEnLista // posicion en el arreglo original
        };
        return b;

    }

    public void crearGrafica() {
        JFreeChart grafica = null;
        DefaultCategoryDataset datos = new DefaultCategoryDataset();
        lbMensaje.setText("Generando, espere: 10%");
        for (int i = 0; i < numeros.size(); i++) {

            if (numeros.get(i)[2] == 1) {
                datos.addValue(numeros.get(i)[0], "Numero", numeros.get(i)[0]);
                datos.addValue(numeros.get(i)[1], "Suma", numeros.get(i)[0]);
                //cuenta++;
            }
            lbMensaje.setText("Generando, espere: 30%");

        }
        switch (cbTipoG.getSelectedIndex()) {

            case 0:
                grafica = ChartFactory.createLineChart("Elementos evaluados: " + evaluados, "Suma de cuadrados de Digitos", "Numeros", datos, PlotOrientation.VERTICAL, true, true, false); //leyenda de que esta graficando, grupo,url
                break;
            case 1:
                grafica = ChartFactory.createBarChart("Elementos evaluados: " + evaluados, "Suma de cuadrados de Digitos", "Numeros", datos, PlotOrientation.VERTICAL, true, true, false); //leyenda de que esta graficando, grupo,url
                break;

            default:
                lbMensaje.setText("Generando, espere: 90%");

        }
        lbMensaje.setText("Generando, espere: 95%");
        ChartPanel cPanel = new ChartPanel(grafica);
        JFrame ventana = new JFrame("Suma de los cuadrados de los digitos de un número , comparado con el número original");
        ventana.getContentPane().add(cPanel);
        ventana.pack();
        ventana.setVisible(true);
        lbMensaje.setText("");

    }

    public void crearGarifaConHilo(JLabel label, int evaluados, ArrayList<Integer[]> numeros, int tipoGrap, JButton ac) {
        GraficaEnHilo graficaH = new GraficaEnHilo(label, evaluados, numeros, tipoGrap, ac);
        graficaH.start();
    }

    private void iniciarArreglos() {
        int max, min;
        max = Integer.valueOf(tFMaximo.getText());
        min = Integer.valueOf(tFMinimo.getText());
        maximo = max;
        minimo = min;
        numeros = new ArrayList<Integer[]>((maximo - minimo) + 1);
        numeros.clear();

    }

    private boolean esCorrectaLaEntrada() {
        StringBuilder error = new StringBuilder();

        boolean esCorrecta = true;

        if (!esEntero(tFMinimo.getText())) {
            error.append("El minimo no es un numero entero");
            error.append("\n");
            esCorrecta = false;
        } else {
            if (Integer.valueOf(tFMinimo.getText()) < 0) {
                error.append("Ingresa un numero positivo en el minimo");
                error.append("\n");
                esCorrecta = false;
            }

        }

        if (!esEntero(tFMaximo.getText())) {
            error.append("El maximo no es un numero entero");
            error.append("\n");
            esCorrecta = false;
        } else {
            if (Integer.valueOf(tFMinimo.getText()) < 0) {
                error.append("Ingresa un numero positivo en el maximo");
                error.append("\n");
                esCorrecta = false;
            }

        }

        if (esEntero(tFMinimo.getText()) && esEntero(tFMaximo.getText())) {

            if (Integer.valueOf(tFMinimo.getText()) >= Integer.valueOf(tFMaximo.getText())) {
                error.append("El maximo debe de ser mayor al minimo");
                error.append("\n");
                esCorrecta = false;
            }

        }

        if (!esCorrecta) {

            JOptionPane.showMessageDialog(this, error, "ERROR", JOptionPane.WARNING_MESSAGE);

        }

        return esCorrecta;

    }

    private boolean esEntero(String cadena) {
        boolean validacion;
        try {
            Integer.parseInt(cadena);
            validacion = true;
        } catch (NumberFormatException excepcion) {
            validacion = false;
        }
        return validacion;
    }

    private void llenarArreglo() {
        for (int i = minimo; i <= maximo; i++) {
            Integer numero[] = new Integer[]{i, 0, 0};
            numeros.add(numero);

        }
    }

    private int[] numeroADigitos(int numero) {

        String numeroStr = String.valueOf(numero);
        int digtitos[] = new int[numeroStr.length()];

        for (int i = 0; i < numeroStr.length(); i++) {
            int j = Character.digit(numeroStr.charAt(i), 10);
            digtitos[i] = j;
        }

        return digtitos;
    }

    private int cuadradoYSuma(int[] digitos) {

        int resultado = 0;

        for (int i = 0; i < digitos.length; i++) {
            resultado += Math.pow(digitos[i], 2);
        }

        return resultado;

    }

    private int ragoBusqueda(int max, int min) {
        int rango = 0;

        int evaluacion = (max - min);
        evaluacion = evaluacion / 10;

        if (evaluacion <= 24) {
            porDeterminista = true;
            return max - min;
            
        }else if (evaluacion <= 100) {// mientras sean menor a mil registros, revisa 100
            rango = 100;
        } else if (evaluacion <= 1000) {//10 mil registros
            rango = 500;
        } else if (evaluacion <= 10000) {// 100 mil 
            rango = 1000;
        } else if (evaluacion <= 100000) {// 1 millon
            rango = 10000;
        } else if (evaluacion <= 1000000) {
            rango = 100000;
        } else if (evaluacion > 1000000) {
            rango = 500000;
        }
        return rango;

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnBuscar = new javax.swing.JButton();
        btnGraficar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tFMaximo = new javax.swing.JTextField();
        tFMinimo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        tFcoincidencias = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        cbTipoG = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        tFevaluados = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        cbTipoAccion = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtAreaResultados = new javax.swing.JTextArea();
        lbMensaje = new javax.swing.JLabel();

        setBackground(new java.awt.Color(137, 192, 199));
        setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));

        btnBuscar.setBackground(new java.awt.Color(51, 153, 255));
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnGraficar.setBackground(new java.awt.Color(0, 204, 102));
        btnGraficar.setText("Graficar");
        btnGraficar.setEnabled(false);
        btnGraficar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGraficarActionPerformed(evt);
            }
        });

        jLabel1.setText("Minimo");

        jLabel2.setText("Maximo");

        jLabel3.setText("Coincidencias");

        tFcoincidencias.setEditable(false);

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(0, 153, 153))); // NOI18N
        jScrollPane1.setOpaque(false);

        jTextArea1.setBackground(new java.awt.Color(0, 102, 102));
        jTextArea1.setColumns(20);
        jTextArea1.setForeground(new java.awt.Color(255, 255, 255));
        jTextArea1.setRows(5);
        jTextArea1.setText("   \n   Buscar: Inicia la busqueda de coincidencias.\n   Graficar: Grafica la relacion entre la entrada numerica \n                   y la suma de los cuadrados de sus digitos.\n   NOTA: minimo y maximo deben de ser enteros positivos,\n              minimo debe de ser mayor a cero.         \n   IMPORTANTE: La grafica tarda más en generarse\n                           entre más grande es el rango de datos.");
        jScrollPane1.setViewportView(jTextArea1);

        jLabel4.setText("TIPO DE GRAFICA");

        cbTipoG.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel5.setText("Elementos Evaluados");

        tFevaluados.setEditable(false);
        tFevaluados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tFevaluadosActionPerformed(evt);
            }
        });

        jLabel6.setText("Metodo De Busqueda");

        cbTipoAccion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jScrollPane2.setViewportBorder(javax.swing.BorderFactory.createTitledBorder("RESULTADOS"));

        txtAreaResultados.setBackground(new java.awt.Color(0, 102, 102));
        txtAreaResultados.setColumns(20);
        txtAreaResultados.setForeground(new java.awt.Color(255, 255, 255));
        txtAreaResultados.setRows(5);
        jScrollPane2.setViewportView(txtAreaResultados);

        lbMensaje.setForeground(new java.awt.Color(255, 255, 204));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbTipoG, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbTipoAccion, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 371, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tFcoincidencias, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(66, 66, 66)
                                .addComponent(tFevaluados, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(12, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(jLabel1)
                        .addGap(33, 33, 33)
                        .addComponent(tFMinimo, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addComponent(jLabel4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tFMaximo, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(105, 105, 105)
                        .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(btnGraficar))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(tFMinimo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(tFMaximo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(cbTipoAccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lbMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnGraficar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(cbTipoG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(24, 24, 24)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tFcoincidencias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tFevaluados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(98, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        inicializarDatos();

    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnGraficarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGraficarActionPerformed
        //crearGrafica();
        crearGarifaConHilo(lbMensaje, evaluados, numeros, cbTipoG.getSelectedIndex(), btnGraficar);
    }//GEN-LAST:event_btnGraficarActionPerformed

    private void tFevaluadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tFevaluadosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tFevaluadosActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnGraficar;
    private javax.swing.JComboBox<String> cbTipoAccion;
    private javax.swing.JComboBox<String> cbTipoG;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel lbMensaje;
    private javax.swing.JTextField tFMaximo;
    private javax.swing.JTextField tFMinimo;
    private javax.swing.JTextField tFcoincidencias;
    private javax.swing.JTextField tFevaluados;
    private javax.swing.JTextArea txtAreaResultados;
    // End of variables declaration//GEN-END:variables
}
