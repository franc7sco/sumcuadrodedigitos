
package sumadedigitosambosmetodos;

import java.awt.BorderLayout;

public class Numeros extends javax.swing.JFrame {

    
    private PanelPrincipal pPrincipal;
    public Numeros(String titulo) {
        this.setTitle(titulo);
        initComponents();
        
    }
    
    public void establecerPanelPrincipal(PanelPrincipal pPrincipal){
    
        this.pPrincipal = pPrincipal;
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        principal = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        principal.setLayout(new java.awt.BorderLayout());
        getContentPane().add(principal, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

   
    public static void main(String args[]) {
            
            
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
               
                Numeros ventana = new Numeros("SumDiCua. V1.0");
                ventana.setSize(725, 450);
                PanelPrincipal pPrincipal = new PanelPrincipal(ventana);
                ventana.establecerPanelPrincipal(pPrincipal);
                ventana.pPrincipal.setVisible(true);
                ventana.principal.add(ventana.pPrincipal,BorderLayout.CENTER);
                ventana.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel principal;
    // End of variables declaration//GEN-END:variables
}
