package sumadedigitosambosmetodos;

import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;


public class GraficaEnHilo extends Thread {

    private JLabel mensaje;
    private int evaluados;
    private ArrayList<Integer[]> numeros;
    private int tipoGrap;
    private JButton ac;
    public GraficaEnHilo(JLabel label,int evaluados,ArrayList<Integer[]> numeros,int tipoGrap,JButton ac) {
        mensaje = label;
        this.evaluados = evaluados;
        this.numeros = numeros;
        this.tipoGrap = tipoGrap;
        this.ac = ac;
    }

    @Override
    public void run() {
        ac.setEnabled(false);
        JFreeChart grafica = null;
        DefaultCategoryDataset datos = new DefaultCategoryDataset();
        mensaje.setText("Generando: 10%");
        for (int i = 0; i < numeros.size(); i++) {

            if (numeros.get(i)[2] == 1) {
                datos.addValue(numeros.get(i)[0], "Numero", numeros.get(i)[0]);
                datos.addValue(numeros.get(i)[1], "Suma", numeros.get(i)[0]);
                //cuenta++;
            }
            mensaje.setText("Generando: 30%");

        }
        switch (tipoGrap) {

            case 0:
                grafica = ChartFactory.createLineChart("Elementos evaluados: " + evaluados, "Suma de cuadrados de Digitos", "Numeros", datos, PlotOrientation.VERTICAL, true, true, false); //leyenda de que esta graficando, grupo,url
                break;
            case 1:
                grafica = ChartFactory.createBarChart("Elementos evaluados: " + evaluados, "Suma de cuadrados de Digitos", "Numeros", datos, PlotOrientation.VERTICAL, true, true, false); //leyenda de que esta graficando, grupo,url
                break;

            default:
                mensaje.setText("Generando: 90%");

        }
        mensaje.setText("Generando: 95%");
        ChartPanel cPanel = new ChartPanel(grafica);
        JFrame ventana = new JFrame("Suma de los cuadrados de los digitos de un número , comparado con el número original");
        ventana.getContentPane().add(cPanel);
        ventana.pack();
        ventana.setVisible(true);
        mensaje.setText("");
        ac.setEnabled(true);
    }

}
