# SumCuadroDeDigitos
El proyecto y las ventanas generadas son propias de netbeans y unicamente se puede editar en con el los archivos de diseño.form, en caso contrario se pierde la consistencia de las ventanas autogeneradas.
Para compilar el proyecto se requiere netbeans 12.x o una version compatible con el manejo de dependencias con maven y su archivo pom.xml
Java 8 o superior.
Para ejecutar el programa tambien se incluye un archivo de tipo jar que se puede lanzar sin el ide y solo teniendo instalado el jre de java 8 o superior.
